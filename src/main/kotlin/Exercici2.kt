class Beguda(val nom: String, var preu: Double, val ensucrada: Boolean) {

    fun buildFullDrink() : String {
        return "$nom $preu $ensucrada"
    }

    init {
        if (ensucrada) preu *= 1.10
    }
}

fun main() {
    val firstElement = Pasta("Croissant", 1.53, 0.200, 500).buildFullPasta()
    val secondElement = Pasta("Ensaimada", 2.10, 0.450, 854).buildFullPasta()
    val thirdElement = Pasta("Donut", 1.00, 0.500, 530).buildFullPasta()

    val firstBeguda = Beguda("Aigua", 1.00, false).buildFullDrink()
    val secondBeguda = Beguda("Cafe tallat", 1.35, false).buildFullDrink()
    val thirdBeguda = Beguda("Té vermell", 1.50, false).buildFullDrink()
    val fourBeguda = Beguda("Cola", 1.65, true).buildFullDrink()

    println("$firstElement \n" +
            "$secondElement \n" +
            "$thirdElement \n" +
            "$firstBeguda \n" +
            "$secondBeguda \n" +
            "$thirdBeguda \n" +
            fourBeguda)
}