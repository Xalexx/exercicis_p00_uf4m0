import java.util.Scanner

class Fuesteria(private val llargada: Int, val amplada: Int, private val preuUnitari: Int){
    val taulellPreuFinal = preuUnitari * (llargada * amplada)
    val llistoPreuFinal = preuUnitari * llargada
}


fun main() {
    val sc = Scanner(System.`in`)

    val numberOfElements = sc.nextInt()
    var preuFinalProductes = 0

    for (pas in 1..numberOfElements){
        val product = sc.next()
        val priceUnit = sc.nextInt()
        val llargada = sc.nextInt()
        preuFinalProductes += if (product.lowercase() == "taulell") {
            val amplada = sc.nextInt()
            Fuesteria(llargada, amplada, priceUnit).taulellPreuFinal
        } else {
            Fuesteria(llargada, amplada = 0, priceUnit).llistoPreuFinal
        }
    }

    println("El preu total és: $preuFinalProductes€")

}
