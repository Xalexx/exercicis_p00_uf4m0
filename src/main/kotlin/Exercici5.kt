data class MechanicalArm(var openAngle: Double, var altitude: Double, var turnedOn : Boolean){

    fun toggle() {
        turnedOn = !turnedOn
        println(MechanicalArm(openAngle, altitude, turnedOn))
    }

    fun updateAltitude(newValue: Int) {
        if (turnedOn) {
            altitude += newValue
            if (altitude < 0.0) altitude = 0.0
            else if (altitude > 30.0) altitude = 30.0
        }
        println(MechanicalArm(openAngle, altitude, turnedOn))
    }

    fun updateAngle(newValue: Int) {
        if (turnedOn) {
            openAngle += newValue
            if (openAngle < 0.0) openAngle = 0.0
            else if (openAngle > 360.0) openAngle = 360.0
        }
        println(MechanicalArm(openAngle, altitude, turnedOn))
    }
}

fun main() {
    val mechanicalArm = MechanicalArm(0.0, 0.0, false)

    mechanicalArm.toggle()
    mechanicalArm.updateAltitude(3)
    mechanicalArm.updateAngle(180)
    mechanicalArm.updateAltitude(-3)
    mechanicalArm.updateAngle(-180)
    mechanicalArm.updateAltitude(3)
    mechanicalArm.toggle()
}