open class Electrodomestic(var preuBase: Int,
                           var color: String = "blanc",
                           var consum: Char = 'G',
                           var pes: Int = 5) {

     open fun preuFinal() : Int {
        when (consum) {
            'A' -> preuBase += 35
            'B' -> preuBase += 30
            'C' -> preuBase += 25
            'D' -> preuBase += 20
            'E' -> preuBase += 15
            'F' -> preuBase += 10
        }
        preuBase += when (pes) {
            in 0..5 -> 0
            in 6..20 -> 20
            in 21..50 -> 50
            in 51..80 -> 80
            else -> 100
        }
        return preuBase
    }
}
class Rentadora(preuBase: Int, var carrega: Int = 5) : Electrodomestic(preuBase) {
    override fun preuFinal() : Int {
        preuBase += when (carrega) {
            in 6..7 -> 55
            8 -> 70
            9 -> 85
            10 -> 100
            else -> 0
        }
        return preuBase
    }
}

class Televisio(preuBase: Int, var tamany: Int = 28) : Electrodomestic(preuBase) {

    override fun preuFinal() : Int {
        preuBase += when (tamany) {
            in 0..31 -> 0
            in 29..32 -> 50
            in 33..42 -> 100
            in 43..50 -> 150
            else -> 200
        }
        return preuBase
    }
}

fun main() {
    val arrayOfElectrodomestics = mutableListOf(
        Electrodomestic(35, "blanc", 'D', 2),
        Electrodomestic(150, "blau", 'G', 45),
        Electrodomestic(20, "vermell", 'A', 15),
        Electrodomestic(100, "groc", 'B', 5),
        Electrodomestic(27, "rosa", 'C', 40),
        Electrodomestic(53, "taronja", 'E', 32),
        Rentadora(670, 5),
        Rentadora(890, 8),
        Televisio(15, 52),
        Televisio(260, 28)
    )

    for (i in arrayOfElectrodomestics.indices){
        println("Electrodomestic ${i + 1}:")
        println(arrayOfElectrodomestics[i].preuBase)
        println(arrayOfElectrodomestics[i].color)
        println(arrayOfElectrodomestics[i].consum)
        println(arrayOfElectrodomestics[i].pes)
        println(arrayOfElectrodomestics[i].preuFinal())
    }

    var preuBaseFinal = 0
    var preuFinalFinal = 0
    println("Electrodomestics:")
    for (i in arrayOfElectrodomestics.indices){
        if (i in 0..5){
            preuBaseFinal += arrayOfElectrodomestics[i].preuBase
            preuFinalFinal += arrayOfElectrodomestics[i].preuFinal()
        }
    }
    println("Preu base: $preuBaseFinal")
    println("Preu final: $preuFinalFinal")

    preuBaseFinal = 0
    preuFinalFinal = 0
    println("Rentadores:")
    for (i in arrayOfElectrodomestics.indices){
        if (i in 6..7){
            preuBaseFinal += arrayOfElectrodomestics[i].preuBase
            preuFinalFinal += arrayOfElectrodomestics[i].preuFinal()
        }
    }

    println("Preu base: $preuBaseFinal")
    println("Preu final: $preuFinalFinal")

    preuBaseFinal = 0
    preuFinalFinal = 0
    println("Televisions:")
    for (i in arrayOfElectrodomestics.indices){
        if (i in 6..7){
            preuBaseFinal += arrayOfElectrodomestics[i].preuBase
            preuFinalFinal += arrayOfElectrodomestics[i].preuFinal()
        }
    }
    println("Preu base: $preuBaseFinal")
    println("Preu final: $preuFinalFinal")
}