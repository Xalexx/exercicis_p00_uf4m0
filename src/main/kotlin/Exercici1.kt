class Pasta(private val nom: String, private val preu: Double, private val pes: Double, private val calories: Int) {
    fun buildFullPasta() : String {
        return "$nom $preu $pes $calories"
    }
}

fun main() {
    val firstElement = Pasta("Croissant", 1.53, 0.200, 500).buildFullPasta()
    val secondElement = Pasta("Ensaimada", 2.10, 0.450, 854).buildFullPasta()
    val thirdElement = Pasta("Donut", 1.00, 0.500, 530).buildFullPasta()

    println("$firstElement \n" +
            "$secondElement \n" +
            thirdElement)
}
