class Lampada(){
    private var indexColor : Int
    private var encendre : Boolean
    private var apagar : Boolean
    private var canviarColor : List<String>
    private var intensidad : Int
    private var auxColor : Int



    init {
        encendre = false
        apagar = true
        canviarColor = listOf("Gris", "Rojo", "Verde", "Azul", "Amarillo", "Black")
        intensidad = 0
        indexColor = 0
        auxColor = 0
    }

    fun turnOn(){
        if (apagar) {
            encendre = true
            intensidad = 1

            indexColor = auxColor

            apagar = false
        }
        println("Color: ${canviarColor[auxColor]} - Intensity: $intensidad")
    }

    fun turnOff(){
        if (encendre) {
            apagar = true
            intensidad = 0

            auxColor = indexColor

            indexColor = canviarColor.lastIndex

            encendre = false
        }
        println("Color: ${canviarColor[indexColor]} - Intensity: $intensidad")
    }

    fun changeColor() {
        if (encendre) {
            if (indexColor == canviarColor.size-2 || auxColor == canviarColor.size-2 ) {
                indexColor = 0
                auxColor = 0
            } else {
                indexColor++
                auxColor = indexColor
            }
        }
        println("Color: ${canviarColor[indexColor]} - Intensity: $intensidad")
    }

    fun intensity(){
        if (encendre) {
            if (intensidad == 5) intensidad = 5
            else intensidad++
        }
        println("Color: ${canviarColor[auxColor]} - Intensity: $intensidad")
    }

}

fun main() {
    val firstLampada = Lampada()
    val secondLampada = Lampada()

    println("Primera lampara:")
    firstLampada.turnOn()
    firstLampada.changeColor()
    firstLampada.changeColor()
    firstLampada.changeColor()
    firstLampada.intensity()
    firstLampada.intensity()
    firstLampada.intensity()
    firstLampada.intensity()


    println("Segona lampara:")
    secondLampada.turnOn()
    secondLampada.changeColor()
    secondLampada.changeColor()
    secondLampada.intensity()
    secondLampada.intensity()
    secondLampada.intensity()
    secondLampada.intensity()
    secondLampada.turnOff()
    secondLampada.changeColor()
    secondLampada.turnOn()
    secondLampada.changeColor()
    secondLampada.intensity()
    secondLampada.intensity()
    secondLampada.intensity()
    secondLampada.intensity()
}